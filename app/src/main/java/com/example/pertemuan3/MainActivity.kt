package com.example.pertemuan3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList

class MainActivity : AppCompatActivity(), View.OnClickListener {
    val COLLECTION = "mahasiswa"
    val F_ID = "id"
    val F_NAMA = "nama"
    val F_ALAMAT = "alamat"
    val F_PHONE = "phone"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alStudent : ArrayList<HashMap<String,Any>>
    lateinit var adapter: SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        alStudent = ArrayList()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }
    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message)
            showData()
        }
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                val hm = HashMap<String,Any>()
                hm.set(F_ID,edId.text.toString())
                hm.set(F_NAMA,edNama.text.toString())
                hm.set(F_ALAMAT,edAlamat.text.toString())
                hm.set(F_PHONE,edPhone.text.toString())
                db.collection(COLLECTION).document(edId.text.toString()).set(hm).
                addOnSuccessListener {
                    Toast.makeText(this, "Data Berhasil Ditambahkan", Toast.LENGTH_SHORT)
                        .show()
                }.addOnFailureListener{ e ->
                    Toast.makeText(this, "Data Gagal Ditambahkan : ${e.message}", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            R.id.btnUpdate ->{
                val hm = HashMap<String,Any>()
                hm.set(F_ID,docId)
                hm.set(F_NAMA,edNama.text.toString())
                hm.set(F_ALAMAT,edAlamat.text.toString())
                hm.set(F_PHONE,edPhone.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Berhasil Diubah", Toast.LENGTH_SHORT)
                            .show() }
                    .addOnFailureListener{ e ->
                        Toast.makeText(this, "Data Gagal Diubah : ${e.message}", Toast.LENGTH_SHORT)
                            .show() }
            }
            R.id.btnDelete ->{
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                        results ->
                    for (doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this, "Data Berhasil Dihapus", Toast.LENGTH_SHORT)
                                    .show()
                            }
                            .addOnFailureListener { e->
                                Toast.makeText(this, "Data Gagal Dihapus ${e.message}", Toast.LENGTH_SHORT)
                                    .show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Tidak dapat mendapatkan data ${e.message}", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alStudent.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edNama.setText(hm.get(F_NAMA).toString())
        edAlamat.setText(hm.get(F_ALAMAT).toString())
        edPhone.setText(hm.get(F_PHONE).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID, doc.get(F_ID).toString())
                hm.set(F_NAMA, doc.get(F_NAMA).toString())
                hm.set(F_ALAMAT, doc.get(F_ALAMAT).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this, alStudent,R.layout.row_data,
                arrayOf(F_ID, F_NAMA, F_ALAMAT, F_PHONE),
                intArrayOf(R.id.txId, R.id.txNama, R.id.txAlamat, R.id.txPhone))
            lsData.adapter = adapter
        }
    }


}